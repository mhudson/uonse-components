package org.uonse.components;

import com.jme3.math.ColorRGBA;
import multiplicity3.appsystem.IQueueOwner;
import multiplicity3.appsystem.MultiplicityClient;
import multiplicity3.csys.behaviours.RotateTranslateScaleBehaviour;
import multiplicity3.csys.factory.ContentTypeAlreadyBoundException;
import multiplicity3.csys.factory.ContentTypeInvalidException;
import multiplicity3.csys.factory.ContentTypeNotBoundException;
import multiplicity3.csys.items.item.IItem;
import multiplicity3.input.MultiTouchInputComponent;
import synergynet3.SynergyNetApp;

import java.util.UUID;

public class LoadingDialogBoxDemo extends SynergyNetApp{

	public static MultiplicityClient client = null;

	public static void main(String[] args) {

		NETWORKING = false;
		client = MultiplicityClient.get();
		client.start();
		client.getContext().getSettings().setTitle("Loading Dialog Box Demo");
		client.setCurrentApp(new LoadingDialogBoxDemo());

	}

	public <ContentType extends IItem, ConcreteType extends IItem> ConcreteType create(Class<ContentType> clazz) {
		UUID id = UUID.randomUUID();
		String name = "id-" + id;
		try {
			return contentFactory.create(clazz, name, id);
		}
		catch (ContentTypeNotBoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void shouldStart(MultiTouchInputComponent input, IQueueOwner iqo) {

		super.shouldStart(input, iqo);
		client.getViewPort().setBackgroundColor(ColorRGBA.Gray);
		registerItems();
		ILoadingDialogBox dialogBox = create(ILoadingDialogBox.class);
		dialogBox.construct(contentFactory);
		RotateTranslateScaleBehaviour rts = behaviourMaker.addBehaviour(dialogBox, RotateTranslateScaleBehaviour.class);
		rts.setScaleLimits(-Float.MAX_VALUE, Float.MAX_VALUE);
//		dialogBox.setRelativeScale(10);
		stage.addItem(dialogBox);

	}

	private void registerItems() {
		try {
			contentFactory.register(ILoadingDialogBox.class, LoadingDialogBox.class);
			contentFactory.register(ILoadingSpinner.class, LoadingSpinner.class);
		}
		catch (ContentTypeAlreadyBoundException e) {
			e.printStackTrace();
		}
		catch (ContentTypeInvalidException e) {
			e.printStackTrace();
		}
	}

}
