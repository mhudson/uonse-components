package org.uonse.components;

import multiplicity3.appsystem.IQueueOwner;
import multiplicity3.appsystem.MultiplicityClient;
import multiplicity3.csys.factory.ContentTypeAlreadyBoundException;
import multiplicity3.csys.factory.ContentTypeInvalidException;
import multiplicity3.csys.factory.ContentTypeNotBoundException;
import multiplicity3.csys.items.item.IItem;
import multiplicity3.input.MultiTouchInputComponent;
import synergynet3.SynergyNetApp;

import java.util.UUID;

public class LoadingSpinnerDemo extends SynergyNetApp {

	public static void main(String[] args) {

		NETWORKING = false;
		MultiplicityClient client = MultiplicityClient.get();
		client.start();
		client.getContext().getSettings().setTitle("Loading Spinner Demo");
		client.setCurrentApp(new LoadingSpinnerDemo());

	}

	public <ContentType extends IItem, ConcreteType extends IItem> ConcreteType create(Class<ContentType> clazz) {
		UUID id = UUID.randomUUID();
		String name = "id-" + id;
		try {
			return this.contentFactory.create(clazz, name, id);
		}
		catch (ContentTypeNotBoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void shouldStart(MultiTouchInputComponent input, IQueueOwner iqo) {

		super.shouldStart(input, iqo);
		registerItems();
		ILoadingSpinner spinner = create(ILoadingSpinner.class);
		this.stage.addItem(spinner);

	}

	private void registerItems() {
		try {
			this.contentFactory.register(ILoadingSpinner.class, LoadingSpinner.class);
		}
		catch (ContentTypeAlreadyBoundException e) {
			e.printStackTrace();
		}
		catch (ContentTypeInvalidException e) {
			e.printStackTrace();
		}
	}


}
