package org.uonse.components;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.system.NanoTimer;
import com.jme3.system.Timer;
import multiplicity3.csys.IUpdateable;
import multiplicity3.csys.annotations.ImplementsContentItem;
import multiplicity3.jme3csys.annotations.RequiresUpdate;
import multiplicity3.jme3csys.geometry.CenteredQuad;
import multiplicity3.jme3csys.items.IInitable;
import multiplicity3.jme3csys.items.item.JMEItem;
import multiplicity3.jme3csys.picking.ItemMap;

import java.util.UUID;

@RequiresUpdate
@ImplementsContentItem(target = ILoadingSpinner.class)
public class LoadingSpinner extends JMEItem implements ILoadingSpinner, IInitable, IUpdateable {

	private float radius = 0.8F;
	private float width = 0.15F;
	private float speed = 1.0F;

	private CenteredQuad quad;
	private Geometry geometry;
	private Material material;
	private Timer timer;
	/**
	 * Instantiates a new JME item.
	 *
	 * @param name the name
	 * @param uuid the uuid
	 */
	public LoadingSpinner(String name, UUID uuid) {
		super(name, uuid);
	}

	/**
	 * Initialize geometry.
	 *
	 * @param assetManager the asset manager
	 */
	public void initializeGeometry(AssetManager assetManager) {
		quad = new CenteredQuad(70, 70);
		geometry = new Geometry(this.getClass().getName(), quad);
		material = new Material(assetManager, "org/uonse/components/LoadingSpinner/LoadingSpinner.j3md");
		material.setFloat("Radius", getRadius());
		material.setFloat("Width", getWidth());
		material.setFloat("Period", 1.0F / getSpeed());
		material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
		geometry.setMaterial(material);
		ItemMap.register(geometry, this);
		attachChild(geometry);
		timer = new NanoTimer();
	}

	/**
	 * Gets the manipulable spatial.
	 *
	 * @return the manipulable spatial
	 */
	public Spatial getManipulableSpatial() {
		return geometry;
	}

	/**
	 * Update.
	 *
	 * @param timePerFrameSeconds the time per frame seconds
	 */
	public void update(float timePerFrameSeconds) {
		material.setFloat("Time", timer.getTimeInSeconds());
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
		material.setFloat("Radius", getRadius());
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
		material.setFloat("Width", getWidth());
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
		material.setFloat("Period", 1.0F / getSpeed());
	}
}
