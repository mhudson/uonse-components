package org.uonse.components;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import multiplicity3.csys.annotations.ImplementsContentItem;
import multiplicity3.csys.factory.ContentTypeNotBoundException;
import multiplicity3.csys.factory.IContentFactory;
import multiplicity3.csys.items.mutablelabel.IMutableLabel;
import multiplicity3.jme3csys.geometry.CenteredQuad;
import multiplicity3.jme3csys.items.IInitable;
import multiplicity3.jme3csys.items.item.JMEItem;
import multiplicity3.jme3csys.picking.ItemMap;

import java.util.UUID;

@ImplementsContentItem(target = ILoadingDialogBox.class)
public class LoadingDialogBox extends JMEItem implements ILoadingDialogBox, IInitable {

	private CenteredQuad quad;
	private Geometry geometry;
	private Material material;
	/**
	 * Instantiates a new JME item.
	 *
	 * @param name the name
	 * @param uuid the uuid
	 */
	public LoadingDialogBox(String name, UUID uuid) {
		super(name, uuid);
	}

	/**
	 * Initialize geometry.
	 *
	 * @param assetManager the asset manager
	 */
	public void initializeGeometry(AssetManager assetManager) {
		addDropShadow(assetManager);
		addBox(assetManager);
	}

	public void addDropShadow(AssetManager assetManager) {
		quad = new CenteredQuad(560, 220);
		geometry = new Geometry(this.getClass().getName(), quad);
		material = new Material(assetManager, "org/uonse/components/Shadow/Shadow.j3md");
		material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
//		material.setFloat("Width", 560);
//		material.setFloat("Height", 220);
		geometry.setMaterial(material);
		geometry.setLocalTranslation(4, -4, 0);
		ItemMap.register(geometry, this);
		attachChild(geometry);
	}

	public void addBox(AssetManager assetManager) {
		quad = new CenteredQuad(560, 220);
		geometry = new Geometry(this.getClass().getName(), quad);
		material = new Material(assetManager, "org/uonse/components/LoadingDialogBox/LoadingDialogBox.j3md");
//		material.setFloat("Width", 560);
//		material.setFloat("Height", 220);
		geometry.setMaterial(material);
		ItemMap.register(geometry, this);
		attachChild(geometry);
	}

	public void construct(IContentFactory contentFactory) {
		try {
			UUID id = UUID.randomUUID();
			String name = "id-" + id;
			IMutableLabel label = contentFactory.create(IMutableLabel.class, name, id);
			label.setText("waddup");
			BitmapText txt = (BitmapText) label.getManipulableSpatial();
			txt.setColor(ColorRGBA.Black);
			addItem(label);

			id = UUID.randomUUID();
			name = "id-" + id;
			ILoadingSpinner spinner = contentFactory.create(ILoadingSpinner.class, name, id);
			spinner.setRelativeLocation(new Vector2f(-180, 0));
			addItem(spinner);


		}
		catch (ContentTypeNotBoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the manipulable spatial.
	 *
	 * @return the manipulable spatial
	 */
	public Spatial getManipulableSpatial() {
		return geometry;
	}
}
