package org.uonse.components;

import multiplicity3.csys.factory.IContentFactory;
import multiplicity3.csys.items.item.IItem;

public interface ILoadingDialogBox extends IItem {

	void construct(IContentFactory contentFactory);

}
