package org.uonse.components;

import multiplicity3.csys.items.item.IItem;

public interface ILoadingSpinner extends IItem {

	float getRadius();
	void setRadius(float radius);
	float getWidth();
	void setWidth(float width);
	float getSpeed();
	void setSpeed(float speed);

}
