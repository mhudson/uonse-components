#define M_PI 3.1415926535897932384626433832795
#define C_BLACK vec4(0.0, 0.0, 0.0, 1.0)
#define C_WHITE vec4(1.0, 1.0, 1.0, 1.0)
#define C_RED vec4(1.0, 0.0, 0.0, 1.0)

uniform float m_Time;
uniform float m_Radius;
uniform float m_Width;
uniform float m_Period;

varying vec2 v_TexCoord;

float angle_to_unit (float angle);
float smooth_bump (float x, float min, float max, float eps);
float and(float a, float b);
float or(float a, float b);
float when_gt(float x, float y);
float when_lt(float x, float y);
float when_ge(float x, float y);
float when_le(float x, float y);

void main () {

	gl_FragColor = vec4(0.1, 0.0, 1.0, 1.0);

	// Convert from uv coordinates [0,1] to [-1,1] with (0,0) as center.
	vec2 object_coord = 2.0 * (v_TexCoord - 0.5);

	float half_width = m_Width / 2.0;
	float min_distance = max(m_Radius - half_width, 0.0);
	float max_distance = m_Radius + half_width;

	float distance_from_center = length(object_coord);
    gl_FragColor.a = smooth_bump(distance_from_center, min_distance, max_distance, fwidth(distance_from_center));
    //gl_FragColor.a = smooth_bump(distance_from_center, min_distance, max_distance, 0.00001);

	float angle = 2.0 * M_PI * m_Time / m_Period;
	mat2 rot = mat2(cos(angle), sin(angle), -sin(angle), cos(angle));
	vec2 rotated_coord = rot * object_coord;

	float angle_unit = angle_to_unit(atan(rotated_coord.y, rotated_coord.x));
	//float angle_unit_dx = fwidth(angle_unit);
	//float angle_unit_smooth = smoothstep(0.0 - angle_unit_dx, 1.0 + angle_unit_dx, angle_unit);
	//float smooth = 1.0 + min(angle_unit_dx, 1.0);
	gl_FragColor.a *= 1.0 - angle_unit;

}

float angle_to_unit (float angle) {
	float angle_half_unit = angle / (2.0 * M_PI);
	return max(angle_half_unit, max(sign(-angle_half_unit), 0.0) * (1.0 + angle_half_unit));
}

float smooth_bump (float x, float min_x, float max_x, float eps) {
    float a = smoothstep(min_x - eps, min_x + eps, x);
    float b = 1.0 - smoothstep(max_x - eps, max_x + eps, x);
    return min(a, b);
}

float and(float a, float b) {
	return a * b;
}

float or(float a, float b) {
	return min(a + b, 1.0);
}

float when_gt(float x, float y) {
  return float(x > y);
}

float when_lt(float x, float y) {
	return float(x < y);
}

float when_ge(float x, float y) {
	return 1.0 - when_lt(x, y);
}

float when_le(float x, float y) {
	return 1.0 - when_gt(x, y);
}
